# SNInAppSettings #

### Your settings inside your iOS app ###

Yep. That's pretty much it.

### Adding it to your project ###

* Add the SNInAppSettings folder to your Xcode project. 

### Example code ###

```javascript
#import "SNInAppSettingsViewController.h"

	UINavigationController *navController =[[UINavigationController alloc] initWithRootViewController:[[SNInAppSettingsViewController alloc] init]];
    navController.modalPresentationStyle = UIModalPresentationFormSheet;
    [self.navigationController presentViewController:navController animated:YES completion:nil];
```