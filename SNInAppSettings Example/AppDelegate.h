//
//  AppDelegate.h
//  SNInAppSettings
//
//  Created by Brian Gerfort of 2ndNature on 07/09/2017.
//  This code is in the public domain.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

