//
//  SNInAppSettingsCell.h
//  SNInAppSettings
//
//  Created by Brian Gerfort of 2ndNature on 07/09/2017.
//  This code is in the public domain.
//

#import "SNInAppSettingsObject.h"

#define ROUNDED_CORNERS_WIDTH_TRIGGER 420.0

@interface SNInAppSettingsCell : UITableViewCell

@property (nonatomic, strong) SNInAppSettingsObject *object;
@property (nonatomic, readonly) UILabel *titleLabel;
@property (nonatomic, assign) BOOL isFirstCellInSection;
@property (nonatomic, assign) BOOL isLastCellInSection;

- (void)preferredContentSizeChanged:(NSNotification *)notification;
- (void)reloadUserDefault;

@end
