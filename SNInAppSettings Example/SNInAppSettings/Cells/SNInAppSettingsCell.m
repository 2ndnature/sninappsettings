//
//  SNInAppSettingsValueCell.m
//  SNInAppSettings
//
//  Created by Brian Gerfort of 2ndNature on 07/09/2017.
//  This code is in the public domain.
//

#import "SNInAppSettingsCell.h"

@implementation SNInAppSettingsCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]))
    {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [_titleLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self.contentView addSubview:_titleLabel];
        
        [[_titleLabel.centerYAnchor constraintEqualToAnchor:self.contentView.centerYAnchor] setActive:YES];
        [[_titleLabel.leftAnchor constraintEqualToAnchor:self.contentView.leftAnchor constant:self.separatorInset.left + 1] setActive:YES];
        [[_titleLabel.rightAnchor constraintLessThanOrEqualToAnchor:self.contentView.rightAnchor constant:-(self.separatorInset.left + 1)] setActive:YES];
        [[_titleLabel.heightAnchor constraintGreaterThanOrEqualToConstant:20.0] setActive:YES];
        [_titleLabel setContentCompressionResistancePriority:751 forAxis:UILayoutConstraintAxisHorizontal];

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(preferredContentSizeChanged:) name:UIContentSizeCategoryDidChangeNotification object:nil];
        [self preferredContentSizeChanged:nil];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)preferredContentSizeChanged:(NSNotification *)notification
{
    [self.titleLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleBody]];
    [self setNeedsLayout];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.object = nil;
    self.accessoryType = UITableViewCellAccessoryNone;
}

- (void)setObject:(SNInAppSettingsObject *)object
{
    if (_object == object) return;
    
    _object = object;
    
    self.titleLabel.text = object.title;
    
    [self reloadUserDefault];
}

- (void)reloadUserDefault
{
    // For the subclass to set its value
}

- (void)setFrame:(CGRect)frame
{
    if (self.superview.bounds.size.width > ROUNDED_CORNERS_WIDTH_TRIGGER)
    {
        frame.origin.x += 20.0;
        frame.size.width -= 40.0;
    }
    [super setFrame:frame];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.layer.mask = nil;
    self.separatorInset = UIEdgeInsetsMake(0.0, 15.0, 0.0, 0.0);
    if (self.superview.bounds.size.width > ROUNDED_CORNERS_WIDTH_TRIGGER)
    {
        UIRectCorner roundCorners = 0;
        CGRect roundRectBounds = self.bounds;
        if (self.isFirstCellInSection)
        {
            roundCorners = UIRectCornerTopLeft|UIRectCornerTopRight;
            roundRectBounds.origin.y += 0.5;
            roundRectBounds.size.height -= 0.5;
        }
        if (self.isLastCellInSection)
        {
            roundCorners = roundCorners|UIRectCornerBottomLeft|UIRectCornerBottomRight;
            roundRectBounds.size.height -= 1.0;
        }
        if (roundCorners)
        {
            UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:roundRectBounds byRoundingCorners:roundCorners cornerRadii:CGSizeMake(4.5, 4.5)];
            CAShapeLayer *maskLayer = [CAShapeLayer layer];
            maskLayer.frame = roundRectBounds;
            maskLayer.path = maskPath.CGPath;
            self.layer.mask = maskLayer;
        }
    }
}

@end
