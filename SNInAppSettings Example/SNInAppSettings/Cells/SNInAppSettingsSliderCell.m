//
//  SNInAppSettingsSliderCell.m
//  SNInAppSettings
//
//  Created by Brian Gerfort of 2ndNature on 07/09/2017.
//  This code is in the public domain.
//

#import "SNInAppSettingsSliderCell.h"

@interface SNInAppSettingsThinSlider : UISlider

@end

@implementation SNInAppSettingsThinSlider

- (CGRect)trackRectForBounds:(CGRect)bounds
{
    CGFloat imageSpacer = (bounds.size.height / 2.0) - 12.0;
    CGRect rect = bounds;
    rect.origin.x = (self.minimumValueImage) ? self.minimumValueImage.size.width + imageSpacer : 0.0;
    rect.origin.y = bounds.size.height / 2.0;
    rect.size.width = bounds.size.width - (rect.origin.x + ((self.maximumValueImage) ? self.maximumValueImage.size.width + imageSpacer : 0.0));
    rect.size.height = 1.0;
    return rect;
}

@end

@implementation SNInAppSettingsSliderCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]))
    {
        _slider = (UISlider *)[[SNInAppSettingsThinSlider alloc] initWithFrame:CGRectZero];
        [_slider setTranslatesAutoresizingMaskIntoConstraints:NO];
        [_slider addTarget:self action:@selector(didEndEditing:) forControlEvents:UIControlEventTouchUpInside];
        [_slider addTarget:self action:@selector(didEndEditing:) forControlEvents:UIControlEventTouchUpOutside];
        [self.contentView addSubview:_slider];
        
        [[_slider.centerYAnchor constraintEqualToAnchor:self.contentView.centerYAnchor] setActive:YES];
        [[_slider.heightAnchor constraintGreaterThanOrEqualToAnchor:self.contentView.heightAnchor] setActive:YES];
        [[_slider.leftAnchor constraintEqualToAnchor:self.contentView.leftAnchor constant:self.separatorInset.left + 1] setActive:YES];
        [[_slider.rightAnchor constraintEqualToAnchor:self.contentView.rightAnchor constant:-(self.separatorInset.left + 1)] setActive:YES];
    }
    return self;
}

- (void)reloadUserDefault
{
    if ([self.object isKindOfClass:[SNInAppSettingsObject class]] && self.object.key.length > 0)
    {
        self.slider.maximumValue = self.object.maximumValue;
        self.slider.minimumValue = self.object.minimumValue;
        self.slider.minimumValueImage = self.object.minimumValueImage;
        self.slider.maximumValueImage = self.object.maximumValueImage;
        self.slider.value = ([[NSUserDefaults standardUserDefaults] objectForKey:self.object.key]) ? [[NSUserDefaults standardUserDefaults] floatForKey:self.object.key] : [self.object.defaultValue floatValue];
    }
    else
    {
        self.slider.value = 0.0f;
    }
}

- (void)didEndEditing:(id)sender
{
    if (self.object.key.length == 0) return;
    
    [[NSUserDefaults standardUserDefaults] setFloat:self.slider.value forKey:self.object.key];
}

@end
