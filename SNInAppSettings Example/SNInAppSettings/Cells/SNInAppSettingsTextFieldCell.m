//
//  SNInAppSettingsTextFieldCell.m
//  SNInAppSettings
//
//  Created by Brian Gerfort of 2ndNature on 07/09/2017.
//  This code is in the public domain.
//

#import "SNInAppSettingsTextFieldCell.h"

@implementation SNInAppSettingsTextFieldCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]))
    {
        _textField = [[UITextField alloc] initWithFrame:CGRectZero];
        [_textField setTranslatesAutoresizingMaskIntoConstraints:NO];
        [_textField addTarget:self action:@selector(didEndEditing:) forControlEvents:UIControlEventEditingDidEnd];
        [self.contentView addSubview:_textField];
        
        [[_textField.centerYAnchor constraintEqualToAnchor:self.contentView.centerYAnchor] setActive:YES];
        [[_textField.heightAnchor constraintGreaterThanOrEqualToAnchor:self.contentView.heightAnchor] setActive:YES];
        [[_textField.rightAnchor constraintEqualToAnchor:self.contentView.rightAnchor constant:-(self.separatorInset.left + 1)] setActive:YES];
        [[_textField.leftAnchor constraintEqualToAnchor:self.titleLabel.rightAnchor constant:10.0] setActive:YES];
        [[_textField.leftAnchor constraintGreaterThanOrEqualToAnchor:self.contentView.leftAnchor constant:116.0] setActive:YES];
        [_textField setContentHuggingPriority:249 forAxis:UILayoutConstraintAxisHorizontal];
        
        [self preferredContentSizeChanged:nil];
    }
    return self;
}

- (void)reloadUserDefault
{
    if ([self.object isKindOfClass:[SNInAppSettingsObject class]] && self.object.key.length > 0)
    {
        self.textField.text = ([[NSUserDefaults standardUserDefaults] objectForKey:self.object.key]) ? [[NSUserDefaults standardUserDefaults] stringForKey:self.object.key] : [self.object.defaultValue description];
        self.textField.keyboardType = self.object.keyboardType;
        self.textField.autocorrectionType = self.object.autocorrectionType;
        self.textField.autocapitalizationType = self.object.autocapitalizationType;
        self.textField.secureTextEntry = self.object.isSecure;
    }
    else
    {
        self.textField.text = @"";
    }
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.textField.delegate = nil;
}

- (void)didEndEditing:(id)sender
{
    if (self.object.key.length == 0) return;
    
    if ([self.textField.text isEqual:([[NSUserDefaults standardUserDefaults] objectForKey:self.object.key]) ? [[NSUserDefaults standardUserDefaults] stringForKey:self.object.key] : [self.object.defaultValue description]] == NO)
    {
        [[NSUserDefaults standardUserDefaults] setObject:self.textField.text forKey:self.object.key];
    }
}

- (void)preferredContentSizeChanged:(NSNotification *)notification
{
    [self.textField setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleBody]];
    
    [super preferredContentSizeChanged:notification];
}

@end
