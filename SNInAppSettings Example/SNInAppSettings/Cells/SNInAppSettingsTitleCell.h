//
//  SNInAppSettingsTitleCell.h
//  SNInAppSettings
//
//  Created by Brian Gerfort of 2ndNature on 07/09/2017.
//  This code is in the public domain.
//

#import "SNInAppSettingsCell.h"

@interface SNInAppSettingsTitleCell : SNInAppSettingsCell

@property (nonatomic, readonly) UILabel *valueLabel;

@end
