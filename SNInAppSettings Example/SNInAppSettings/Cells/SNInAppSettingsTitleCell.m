//
//  SNInAppSettingsTitleCell.m
//  SNInAppSettings
//
//  Created by Brian Gerfort of 2ndNature on 07/09/2017.
//  This code is in the public domain.
//

#import "SNInAppSettingsTitleCell.h"

@implementation SNInAppSettingsTitleCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if ((self = [super initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:reuseIdentifier]))
    {
        _valueLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [_valueLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
        [_valueLabel setTextColor:[UIColor colorWithRed:142.0/255.0 green:142.0/255.0 blue:147.0/255.0 alpha:1.0]];
        [_valueLabel setTextAlignment:NSTextAlignmentRight];
        [self.contentView addSubview:_valueLabel];
        
        [[_valueLabel.centerYAnchor constraintEqualToAnchor:self.contentView.centerYAnchor] setActive:YES];
        [[_valueLabel.heightAnchor constraintGreaterThanOrEqualToConstant:20.0] setActive:YES];
        [[_valueLabel.rightAnchor constraintEqualToAnchor:self.contentView.rightAnchor constant:-(self.separatorInset.left + 1)] setActive:YES];
        [[_valueLabel.leftAnchor constraintEqualToAnchor:self.titleLabel.rightAnchor constant:10.0] setActive:YES];
        [[_valueLabel.leftAnchor constraintGreaterThanOrEqualToAnchor:self.contentView.leftAnchor constant:116.0] setActive:YES];
        [_valueLabel setContentHuggingPriority:249 forAxis:UILayoutConstraintAxisHorizontal];
        
        [self preferredContentSizeChanged:nil];
    }
    return self;
}

- (void)reloadUserDefault
{
    if ([self.object isKindOfClass:[SNInAppSettingsObject class]] && self.object.key.length > 0)
    {
        if (self.object.values.count > 0)
        {
            id value = ([[NSUserDefaults standardUserDefaults] objectForKey:self.object.key]) ? [[NSUserDefaults standardUserDefaults] objectForKey:self.object.key] : self.object.defaultValue;
            self.valueLabel.text = (self.object.values.count == self.object.titles.count) ? self.object.titles[([self.object.values indexOfObject:value] < self.object.titles.count) ? [self.object.values indexOfObject:value] : 0] : @"";
        }
        else
        {
            self.valueLabel.text = ([[NSUserDefaults standardUserDefaults] objectForKey:self.object.key]) ? ([[NSUserDefaults standardUserDefaults] objectForKey:self.object.key]) : [self.object.defaultValue description];
        }
    }
    else
    {
        self.valueLabel.text = @"";
    }
}

- (void)preferredContentSizeChanged:(NSNotification *)notification
{
    [self.valueLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleBody]];
    
    [super preferredContentSizeChanged:notification];
}

@end
