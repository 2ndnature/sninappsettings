//
//  SNInAppSettingsToggleSwitchCell.m
//  SNInAppSettings
//
//  Created by Brian Gerfort of 2ndNature on 07/09/2017.
//  This code is in the public domain.
//

#import "SNInAppSettingsToggleSwitchCell.h"

@implementation SNInAppSettingsToggleSwitchCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]))
    {
        _toggleSwitch = [[UISwitch alloc] initWithFrame:CGRectZero];
        [_toggleSwitch addTarget:self action:@selector(switchedToggled:) forControlEvents:UIControlEventValueChanged];
        self.accessoryView = _toggleSwitch;
    }
    return self;
}

- (void)reloadUserDefault
{
    if ([self.object isKindOfClass:[SNInAppSettingsObject class]] && self.object.key.length > 0)
    {
        [self.toggleSwitch setOn:([[NSUserDefaults standardUserDefaults] objectForKey:self.object.key]) ? [[NSUserDefaults standardUserDefaults] boolForKey:self.object.key] : [self.object.defaultValue integerValue]];
    }
    else
    {
        [self.toggleSwitch setOn:NO];
    }
}

- (void)switchedToggled:(id)sender
{
    if (self.object.key.length == 0) return;
    
    [[NSUserDefaults standardUserDefaults] setBool:self.toggleSwitch.isOn forKey:self.object.key];
}

@end
