//
//  SNInAppSettingsMultiValuePicker.h
//  SNInAppSettings
//
//  Created by Brian Gerfort of 2ndNature on 07/09/2017.
//  This code is in the public domain.
//

#import <UIKit/UIKit.h>

@class SNInAppSettingsObject;

@interface SNInAppSettingsMultiValuePicker : UITableViewController

- (instancetype)initWithObject:(SNInAppSettingsObject *)object;

@end
