//
//  SNInAppSettingsMultiValuePicker.m
//  SNInAppSettings
//
//  Created by Brian Gerfort of 2ndNature on 07/09/2017.
//  This code is in the public domain.
//

#import "SNInAppSettingsMultiValuePicker.h"
#import "SNInAppSettingsCell.h"
#import "SNInAppSettingsObject.h"

@interface SNInAppSettingsMultiValuePicker ()

@property (nonatomic, strong) SNInAppSettingsObject *object;
@property (nonatomic, assign) NSUInteger selectedRow;

@end

@implementation SNInAppSettingsMultiValuePicker

static NSString *SNInAppSettingsMultiValuePickerCellIdentifier = @"SNInAppSettingsMultiValuePickerCellIdentifier";

- (instancetype)initWithObject:(SNInAppSettingsObject *)object
{
    if (([object isKindOfClass:[SNInAppSettingsObject class]] && object.key.length > 0 && object.values.count > 0 && object.values.count == object.titles.count) == NO) return nil;
    
    if ((self = [super initWithStyle:UITableViewStyleGrouped]))
    {
        self.object = object;
        [self refreshPick];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationItem.title = self.object.title;
    
    [self.tableView registerClass:[SNInAppSettingsCell class] forCellReuseIdentifier:SNInAppSettingsMultiValuePickerCellIdentifier];
    [self.tableView setEstimatedRowHeight:44.0];
    [self.tableView setRowHeight:UITableViewAutomaticDimension];
    [self updateLayoutMarginsForSize:self.tableView.frame.size];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [self updateLayoutMarginsForSize:size];
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
}

- (void)updateLayoutMarginsForSize:(CGSize)size
{
    if ([[[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."] firstObject] integerValue] < 11) // Insets wonk prior to iOS 11
    {
        [self.tableView setLayoutMargins:(size.width > ROUNDED_CORNERS_WIDTH_TRIGGER) ? UIEdgeInsetsMake(0.0, 20.0, 0.0, 0.0) : UIEdgeInsetsZero];
    }
    else
    {
        [self.tableView setLayoutMargins:(size.width > ROUNDED_CORNERS_WIDTH_TRIGGER) ? UIEdgeInsetsMake(0.0, 34.0, 0.0, 34.0) : UIEdgeInsetsZero];
    }
}

- (void)refreshPick
{
    id selectedValue = ([[NSUserDefaults standardUserDefaults] objectForKey:self.object.key]) ? [[NSUserDefaults standardUserDefaults] objectForKey:self.object.key] : self.object.defaultValue;
    self.selectedRow = ([self.object.values indexOfObject:selectedValue] < self.object.titles.count) ? [self.object.values indexOfObject:selectedValue] : 0;
}

#pragma mark - UITableView DataSource/Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.object.values.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SNInAppSettingsCell *cell = [tableView dequeueReusableCellWithIdentifier:SNInAppSettingsMultiValuePickerCellIdentifier forIndexPath:indexPath];
    cell.titleLabel.text = self.object.titles[indexPath.row];
    cell.accessoryType = (indexPath.row == self.selectedRow) ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    cell.isFirstCellInSection = (indexPath.row == 0);
    cell.isLastCellInSection = (indexPath.row == self.object.values.count - 1);
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[NSUserDefaults standardUserDefaults] setObject:self.object.values[indexPath.row] forKey:self.object.key];
    [self refreshPick];
    [tableView reloadRowsAtIndexPaths:[tableView indexPathsForVisibleRows] withRowAnimation:UITableViewRowAnimationFade];
}

@end
