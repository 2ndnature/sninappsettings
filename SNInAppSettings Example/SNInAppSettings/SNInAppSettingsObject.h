//
//  SNInAppSettingsObject.h
//  SNInAppSettings
//
//  Created by Brian Gerfort of 2ndNature on 07/09/2017.
//  This code is in the public domain.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    SNInAppSettingsObjectTypeChildPane,
    SNInAppSettingsObjectTypeToggleSwitch,
    SNInAppSettingsObjectTypeSlider,
    SNInAppSettingsObjectTypeTitle,
    SNInAppSettingsObjectTypeTextField,
    SNInAppSettingsObjectTypeMultiValue,
    SNInAppSettingsObjectTypeRadioElement,
} SNInAppSettingsObjectType;

@interface SNInAppSettingsObject : NSObject

@property (nonatomic, assign) SNInAppSettingsObjectType type;
@property (nonatomic, copy) NSString *key;
@property (nonatomic, strong) id defaultValue;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, strong) NSArray<NSString *> *titles;
@property (nonatomic, strong) NSArray<id> *values;
@property (nonatomic, assign) BOOL isSecure;
@property (nonatomic, assign) float minimumValue;
@property (nonatomic, assign) float maximumValue;
@property (nonatomic, strong) UIImage *minimumValueImage;
@property (nonatomic, strong) UIImage *maximumValueImage;
@property (nonatomic, copy) NSString *file;
@property (nonatomic, assign) UIKeyboardType keyboardType;
@property (nonatomic, assign) UITextAutocapitalizationType autocapitalizationType;
@property (nonatomic, assign) UITextAutocorrectionType autocorrectionType;

@end
