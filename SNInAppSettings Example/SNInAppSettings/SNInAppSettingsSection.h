//
//  SNInAppSettingsSection.h
//  SNInAppSettings
//
//  Created by Brian Gerfort of 2ndNature on 07/09/2017.
//  This code is in the public domain.
//

#import <Foundation/Foundation.h>

@class SNInAppSettingsObject;

@interface SNInAppSettingsSection : NSObject

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *footerText;
@property (nonatomic, strong) id defaultValue;
@property (nonatomic, strong) NSMutableArray<SNInAppSettingsObject *> *rows;

@end
