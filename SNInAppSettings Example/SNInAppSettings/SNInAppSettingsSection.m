//
//  SNInAppSettingsSection.m
//  SNInAppSettings
//
//  Created by Brian Gerfort of 2ndNature on 07/09/2017.
//  This code is in the public domain.
//

#import "SNInAppSettingsSection.h"

@implementation SNInAppSettingsSection

- (instancetype)init
{
    if ((self = [super init]))
    {
        self.rows = [NSMutableArray array];
    }
    return self;
}

@end
