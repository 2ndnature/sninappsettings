//
//  SNInAppSettingsViewController.m
//  SNInAppSettings
//
//  Created by Brian Gerfort of 2ndNature on 07/09/2017.
//  This code is in the public domain.
//
// https://developer.apple.com/library/content/documentation/PreferenceSettings/Conceptual/SettingsApplicationSchemaReference/Introduction/Introduction.html#//apple_ref/doc/uid/TP40007005-SW1

#import "SNInAppSettingsViewController.h"
#import "SNInAppSettingsSection.h"
#import "SNInAppSettingsToggleSwitchCell.h"
#import "SNInAppSettingsTextFieldCell.h"
#import "SNInAppSettingsMultiValueCell.h"
#import "SNInAppSettingsMultiValuePicker.h"
#import "SNInAppSettingsTitleCell.h"
#import "SNInAppSettingsSliderCell.h"

@interface SNInAppSettingsViewController () <UITextFieldDelegate>

@property (nonatomic, copy) NSString *settingsName;
@property (nonatomic, strong) NSBundle *settingsBundle;
@property (nonatomic, copy) NSString *localizationTable;
@property (nonatomic, strong) NSArray<SNInAppSettingsSection *> *sections;
@property (nonatomic, weak) UITextField *activeTextField;

@end

@implementation SNInAppSettingsViewController

static NSString *SNInAppSettingsCellIdentifier = @"SNInAppSettingsCellIdentifier";
static NSString *SNInAppSettingsToggleSwitchCellIdentifier = @"SNInAppSettingsToggleSwitchCellIdentifier";
static NSString *SNInAppSettingsSliderCellIdentifier = @"SNInAppSettingsSliderCellIdentifier";
static NSString *SNInAppSettingsTitleCellIdentifier = @"SNInAppSettingsTitleCellIdentifier";
static NSString *SNInAppSettingsTextFieldCellIdentifier = @"SNInAppSettingsTextFieldCellIdentifier";
static NSString *SNInAppSettingsMultiValueCellIdentifier = @"SNInAppSettingsMultiValueCellIdentifier";

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)init
{
    return [self initWithSettingsNamed:nil title:NSLocalizedString(@"Settings", NULL)];
}

- (instancetype)initWithSettingsNamed:(NSString *)plistName title:(NSString *)title
{
    if ((self = [super initWithStyle:UITableViewStyleGrouped]))
    {
        self.title = title;
        self.settingsBundle = [NSBundle bundleWithPath:[[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"Settings.bundle"]];
        self.settingsName = (plistName) ? plistName : @"Root";
        [self reloadSettings];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.presentationController != nil && self.navigationController.viewControllers.firstObject == self)
    {
        self.navigationItem.rightBarButtonItems = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismiss:)]];
    }
    
    [self.tableView registerClass:[SNInAppSettingsToggleSwitchCell class] forCellReuseIdentifier:SNInAppSettingsToggleSwitchCellIdentifier];
    [self.tableView registerClass:[SNInAppSettingsTextFieldCell class] forCellReuseIdentifier:SNInAppSettingsTextFieldCellIdentifier];
    [self.tableView registerClass:[SNInAppSettingsMultiValueCell class] forCellReuseIdentifier:SNInAppSettingsMultiValueCellIdentifier];
    [self.tableView registerClass:[SNInAppSettingsTitleCell class] forCellReuseIdentifier:SNInAppSettingsTitleCellIdentifier];
    [self.tableView registerClass:[SNInAppSettingsSliderCell class] forCellReuseIdentifier:SNInAppSettingsSliderCellIdentifier];
    [self.tableView registerClass:[SNInAppSettingsCell class] forCellReuseIdentifier:SNInAppSettingsCellIdentifier];
    [self.tableView setEstimatedRowHeight:44.0];
    [self.tableView setRowHeight:UITableViewAutomaticDimension];
    [self updateLayoutMarginsForSize:self.tableView.frame.size];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDefaultsDidChange:) name:NSUserDefaultsDidChangeNotification object:nil];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [self updateLayoutMarginsForSize:size];
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
}

#pragma mark -

- (void)updateLayoutMarginsForSize:(CGSize)size
{
    if ([[[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."] firstObject] integerValue] < 11) // Insets wonk prior to iOS 11
    {
        [self.tableView setLayoutMargins:(size.width > ROUNDED_CORNERS_WIDTH_TRIGGER) ? UIEdgeInsetsMake(0.0, 20.0, 0.0, 0.0) : UIEdgeInsetsZero];
    }
    else
    {
        [self.tableView setLayoutMargins:(size.width > ROUNDED_CORNERS_WIDTH_TRIGGER) ? UIEdgeInsetsMake(0.0, 34.0, 0.0, 34.0) : UIEdgeInsetsZero];
    }
}

- (void)userDefaultsDidChange:(NSNotification *)notification
{
    [[self.tableView visibleCells] makeObjectsPerformSelector:@selector(reloadUserDefault)];
}

- (void)dismiss:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (NSString *)localizedString:(NSString *)string
{
    return ([string isKindOfClass:[NSString class]]) ? [self.settingsBundle localizedStringForKey:string value:string table:self.localizationTable] : [string description];
}

- (BOOL)reloadSettings
{
    NSMutableArray<SNInAppSettingsSection *> *sections = nil;
    NSString *basePath = [self.settingsBundle bundlePath];
    NSString *plistPath = [basePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@.plist", self.settingsName, (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? @"~ipad" : @"~iphone"]];
    NSDictionary *settingsDictionary = ([[NSFileManager defaultManager] fileExistsAtPath:plistPath]) ? [NSDictionary dictionaryWithContentsOfFile:plistPath] : nil;
    if (!settingsDictionary)
    {
        plistPath = [basePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.plist", self.settingsName]];
        settingsDictionary = ([[NSFileManager defaultManager] fileExistsAtPath:plistPath]) ? [NSDictionary dictionaryWithContentsOfFile:plistPath] : nil;
    }
    if (settingsDictionary)
    {
        self.localizationTable = [settingsDictionary objectForKey:@"StringsTable"];
        if (self.localizationTable == nil)
        {
            self.localizationTable = self.settingsName;
            if([self.settingsBundle pathForResource:self.localizationTable ofType:@"strings"] == nil)
            {
                self.localizationTable = @"Root";
            }
        }
        NSString *userInterfaceIdiom = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? @"Pad" : @"Phone";
        sections = [NSMutableArray arrayWithCapacity:settingsDictionary.count];
        SNInAppSettingsSection *currentSection = [[SNInAppSettingsSection alloc] init];
        NSString *type, *keyboardType, *autocorrectionType, *autocapitalizationType;
        for (NSDictionary *specifier in [settingsDictionary objectForKey:@"PreferenceSpecifiers"])
        {
            if ([specifier isKindOfClass:[NSDictionary class]])
            {
                if ((type = [specifier objectForKey:@"Type"]))
                {
                    if ([specifier objectForKey:@"SupportedUserInterfaceIdioms"] == nil ||
                        ([[specifier objectForKey:@"SupportedUserInterfaceIdioms"] isKindOfClass:[NSArray class]] && [[specifier objectForKey:@"SupportedUserInterfaceIdioms"] containsObject:userInterfaceIdiom]))
                    {
                        if ([type isEqual:@"PSGroupSpecifier"])
                        {
                            if (currentSection.rows.count > 0)
                            {
                                [sections addObject:currentSection];
                            }
                            currentSection = [[SNInAppSettingsSection alloc] init];
                            currentSection.title = [self localizedString:[specifier objectForKey:@"Title"]];
                            currentSection.footerText = [self localizedString:[specifier objectForKey:@"FooterText"]];
                        }
                        else if ([type isEqual:@"PSRadioGroupSpecifier"])
                        {
                            if ([specifier objectForKey:@"Titles"] && [specifier objectForKey:@"Values"])
                            {
                                NSArray<NSString *> *titles = [specifier objectForKey:@"Titles"];
                                NSArray<id> *values = [specifier objectForKey:@"Values"];
                                if (titles.count == values.count && titles.count > 0)
                                {
                                    if (currentSection.rows.count > 0)
                                    {
                                        [sections addObject:currentSection];
                                    }
                                    currentSection = [[SNInAppSettingsSection alloc] init];
                                    currentSection.title = [self localizedString:[specifier objectForKey:@"Title"]];
                                    currentSection.footerText = [self localizedString:[specifier objectForKey:@"FooterText"]];
                                    currentSection.defaultValue = [specifier objectForKey:@"DefaultValue"];
                                    for (NSUInteger idx = 0; idx < titles.count; idx++)
                                    {
                                        SNInAppSettingsObject *row = [[SNInAppSettingsObject alloc] init];
                                        row.title = [self localizedString:titles[idx]];
                                        row.type = SNInAppSettingsObjectTypeRadioElement;
                                        row.key = [specifier objectForKey:@"Key"];
                                        row.defaultValue = values[idx];
                                        [currentSection.rows addObject:row];
                                    }
                                    if ([specifier objectForKey:@"DisplaySortedByTitle"])
                                    {
                                        [currentSection.rows sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                                            return [obj1 compare:obj2];
                                        }];
                                    }
                                }
                            }
                        }
                        else if ([type isEqual:@"PSChildPaneSpecifier"])
                        {
                            SNInAppSettingsObject *row = [[SNInAppSettingsObject alloc] init];
                            row.title = [self localizedString:[specifier objectForKey:@"Title"]];
                            row.file = ([specifier objectForKey:@"File"]) ? [specifier objectForKey:@"File"] : nil;
                            row.type = SNInAppSettingsObjectTypeChildPane;
                            [currentSection.rows addObject:row];
                        }
                        else if ([specifier objectForKey:@"DefaultValue"])
                        {
                            SNInAppSettingsObject *row = [[SNInAppSettingsObject alloc] init];
                            row.title = [self localizedString:[specifier objectForKey:@"Title"]];
                            row.key = [specifier objectForKey:@"Key"];
                            row.defaultValue = [specifier objectForKey:@"DefaultValue"];
                            row.isSecure = [[specifier objectForKey:@"IsSecure"] integerValue];
                            row.values = [specifier objectForKey:@"Values"];
                            row.maximumValue = [[specifier objectForKey:@"MaximumValue"] floatValue];
                            row.minimumValue = [[specifier objectForKey:@"MinimumValue"] floatValue];
                            row.minimumValueImage = ([specifier objectForKey:@"MinimumValueImage"]) ? [UIImage imageWithContentsOfFile:[[self.settingsBundle bundlePath] stringByAppendingPathComponent:[specifier objectForKey:@"MinimumValueImage"]]] : nil;
                            row.maximumValueImage = ([specifier objectForKey:@"MaximumValueImage"]) ? [UIImage imageWithContentsOfFile:[[self.settingsBundle bundlePath] stringByAppendingPathComponent:[specifier objectForKey:@"MaximumValueImage"]]] : nil;
                            if ([[specifier objectForKey:@"Titles"] isKindOfClass:[NSArray class]])
                            {
                                NSArray<NSString *> *titles = [specifier objectForKey:@"Titles"];
                                NSMutableArray<NSString *> *localizedTitles = [NSMutableArray arrayWithCapacity:titles.count];
                                for (NSString *title in titles)
                                {
                                    [localizedTitles addObject:[self localizedString:title]];
                                }
                                row.titles = localizedTitles;
                            }
                            if ((keyboardType = [specifier objectForKey:@"KeyboardType"]))
                            {
                                if ([keyboardType isEqual:@"Alphabet"]) row.keyboardType = UIKeyboardTypeAlphabet;
                                else if ([keyboardType isEqual:@"NumbersAndPunctuation"]) row.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
                                else if ([keyboardType isEqual:@"NumberPad"]) row.keyboardType = UIKeyboardTypeNumberPad;
                                else if ([keyboardType isEqual:@"URL"]) row.keyboardType = UIKeyboardTypeURL;
                                else if ([keyboardType isEqual:@"EmailAddress"]) row.keyboardType = UIKeyboardTypeEmailAddress;
                            }
                            if ((autocorrectionType = [specifier objectForKey:@"AutocorrectionType"]))
                            {
                                if ([keyboardType isEqual:@"No"]) row.autocorrectionType = UITextAutocorrectionTypeNo;
                                else if ([keyboardType isEqual:@"Yes"]) row.autocorrectionType = UITextAutocorrectionTypeYes;
                                else row.autocorrectionType = UITextAutocorrectionTypeDefault;
                            }
                            if ((autocapitalizationType = [specifier objectForKey:@"AutocapitalizationType"]))
                            {
                                if ([keyboardType isEqual:@"None"]) row.autocapitalizationType = UITextAutocapitalizationTypeNone;
                                else if ([keyboardType isEqual:@"Sentences"]) row.autocapitalizationType = UITextAutocapitalizationTypeSentences;
                                else if ([keyboardType isEqual:@"Words"]) row.autocapitalizationType = UITextAutocapitalizationTypeWords;
                                else if ([keyboardType isEqual:@"AllCharacters"]) row.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
                            }
                            if ([type isEqual:@"PSToggleSwitchSpecifier"]) row.type = SNInAppSettingsObjectTypeToggleSwitch;
                            else if ([type isEqual:@"PSTextFieldSpecifier"]) row.type = SNInAppSettingsObjectTypeTextField;
                            else if ([type isEqual:@"PSMultiValueSpecifier"]) row.type = SNInAppSettingsObjectTypeMultiValue;
                            else if ([type isEqual:@"PSTitleValueSpecifier"]) row.type = SNInAppSettingsObjectTypeTitle;
                            else if ([type isEqual:@"PSSliderSpecifier"]) row.type = SNInAppSettingsObjectTypeSlider;
                            [currentSection.rows addObject:row];
                        }
                    }
                }
            }
        }
        if (currentSection.rows.count > 0)
        {
            [sections addObject:currentSection];
        }
    }
    self.sections = (sections) ? [NSArray arrayWithArray:sections] : @[];
    return NO;
}

#pragma mark - UITableView DataSource/Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.sections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.sections[section].rows.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return self.sections[section].title;
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return self.sections[section].footerText;
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    SNInAppSettingsObject *object = self.sections[indexPath.section].rows[indexPath.row];
    switch (object.type)
    {
        case SNInAppSettingsObjectTypeMultiValue:
        case SNInAppSettingsObjectTypeChildPane:
        case SNInAppSettingsObjectTypeRadioElement:
            [self.activeTextField resignFirstResponder];
            return YES;
        case SNInAppSettingsObjectTypeTextField:
            return NO;
        default:
            [self.activeTextField resignFirstResponder];
            return NO;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SNInAppSettingsCell *cell;
    SNInAppSettingsObject *object = self.sections[indexPath.section].rows[indexPath.row];
    switch (object.type)
    {
        case SNInAppSettingsObjectTypeToggleSwitch:
            cell = [tableView dequeueReusableCellWithIdentifier:SNInAppSettingsToggleSwitchCellIdentifier forIndexPath:indexPath];
            break;
        case SNInAppSettingsObjectTypeTextField:
            cell = [tableView dequeueReusableCellWithIdentifier:SNInAppSettingsTextFieldCellIdentifier forIndexPath:indexPath];
            [[(SNInAppSettingsTextFieldCell *)cell textField] setDelegate:self];
            break;
        case SNInAppSettingsObjectTypeMultiValue:
            cell = [tableView dequeueReusableCellWithIdentifier:SNInAppSettingsMultiValueCellIdentifier forIndexPath:indexPath];
            break;
        case SNInAppSettingsObjectTypeTitle:
            cell = [tableView dequeueReusableCellWithIdentifier:SNInAppSettingsTitleCellIdentifier forIndexPath:indexPath];
            break;
        case SNInAppSettingsObjectTypeSlider:
            cell = [tableView dequeueReusableCellWithIdentifier:SNInAppSettingsSliderCellIdentifier forIndexPath:indexPath];
            break;
        case SNInAppSettingsObjectTypeChildPane:
            cell = [tableView dequeueReusableCellWithIdentifier:SNInAppSettingsCellIdentifier forIndexPath:indexPath];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            break;
        case SNInAppSettingsObjectTypeRadioElement:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:SNInAppSettingsCellIdentifier forIndexPath:indexPath];
            cell.accessoryType = ([object.defaultValue isEqual:([[NSUserDefaults standardUserDefaults] objectForKey:object.key]) ? [[NSUserDefaults standardUserDefaults] objectForKey:object.key] : self.sections[indexPath.section].defaultValue]) ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
        }
    }
    cell.object = object;
    cell.isFirstCellInSection = (indexPath.row == 0);
    cell.isLastCellInSection = (indexPath.row == self.sections[indexPath.section].rows.count - 1);
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SNInAppSettingsObject *object = self.sections[indexPath.section].rows[indexPath.row];
    switch (object.type)
    {
        case SNInAppSettingsObjectTypeMultiValue:
            [self.navigationController pushViewController:[[SNInAppSettingsMultiValuePicker alloc] initWithObject:object] animated:YES];
            break;
        case SNInAppSettingsObjectTypeChildPane:
            [self.navigationController pushViewController:[[SNInAppSettingsViewController alloc] initWithSettingsNamed:object.file title:object.title] animated:YES];
            break;
        case SNInAppSettingsObjectTypeRadioElement:
            [[NSUserDefaults standardUserDefaults] setObject:object.defaultValue forKey:object.key];
            [tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
        default:
            break;
    }
}

#pragma mark - UITextField Delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.activeTextField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.activeTextField = nil;
}

@end
