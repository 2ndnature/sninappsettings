//
//  ViewController.m
//  SNInAppSettings
//
//  Created by Brian Gerfort of 2ndNature on 07/09/2017.
//  This code is in the public domain.
//

#import "ViewController.h"
#import "SNInAppSettingsViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Example", NULL);
    self.view.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
    self.navigationItem.leftBarButtonItems = @[[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Settings", NULL) style:UIBarButtonItemStylePlain target:self action:@selector(introduceSettings:)]];
}

- (void)introduceSettings:(id)sender
{
    UINavigationController *navController =[[UINavigationController alloc] initWithRootViewController:[[SNInAppSettingsViewController alloc] init]];
    navController.modalPresentationStyle = UIModalPresentationFormSheet;
    [self.navigationController presentViewController:navController animated:YES completion:nil];
}

@end
